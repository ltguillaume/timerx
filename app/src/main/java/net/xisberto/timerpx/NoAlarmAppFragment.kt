package net.xisberto.timerpx

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import net.xisberto.timerpx.databinding.NoAlarmAppBinding

class NoAlarmAppFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = NoAlarmAppBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.btnInstallFdroid.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://search?q=alarm")
                )
            )
        }

        return view
    }
}
package net.xisberto.timerpx.database

import androidx.room.*

@Dao
interface TimerWidgetDao {

    @Transaction
    @Query("SELECT * FROM TimerWidget WHERE appWidgetId = :appWidgetId")
    fun get(appWidgetId: Int): WidgetAndDef?

    @Insert
    fun insert(timerWidget: TimerWidget): Long

    @Delete
    fun delete(timerWidget: TimerWidget)
}
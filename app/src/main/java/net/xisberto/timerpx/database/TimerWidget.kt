package net.xisberto.timerpx.database

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity(
    indices = [Index(value = ["timerDefId"])],
    foreignKeys = [ForeignKey(entity = TimerDef::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("timerDefId"),
        onDelete = ForeignKey.CASCADE
    )]
)
data class TimerWidget(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val appWidgetId: Int = 0,
    val timerDefId: Int = 0
)

data class WidgetAndDef(
    @Embedded val widget: TimerWidget,
    @Relation(
        parentColumn = "timerDefId",
        entityColumn = "id"
    )
    val timerDef: TimerDef
)
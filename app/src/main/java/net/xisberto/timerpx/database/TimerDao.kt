package net.xisberto.timerpx.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TimerDao {
    companion object {
        const val COLUMN_LABEL = "label"
        const val COLUMN_DURATION = "duration"
    }

    @Query("SELECT * FROM TimerDef ORDER BY label")
    fun getTimersOrderByLabel(): LiveData<List<TimerDef>>

    @Query("SELECT * FROM TimerDef ORDER BY duration")
    fun getTimersOrderByDuration(): LiveData<List<TimerDef>>

    @Query("SELECT * from TimerDef WHERE duration = :duration")
    fun get(duration: Long): LiveData<TimerDef>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(vararg timer: TimerDef)

    @Delete
    fun delete(vararg timer: TimerDef)
}
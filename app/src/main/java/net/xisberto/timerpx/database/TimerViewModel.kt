package net.xisberto.timerpx.database

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TimerViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: TimerRepository
    private val orderCriteria = MutableLiveData(TimerDao.COLUMN_LABEL)
    val allTimers: LiveData<List<TimerDef>>
    val action = MutableLiveData<Int>()
    val selection = MutableLiveData<TimerDef>()

    init {
        val database = Database.getDatabase(application.applicationContext)
        repository = TimerRepository(database)
        allTimers = orderCriteria.switchMap {
            when (it) {
                TimerDao.COLUMN_DURATION -> repository.getAllTimers(it)
                else -> repository.getAllTimers()
            }
        }
    }

    fun orderTimers(value: String? = TimerDao.COLUMN_LABEL) {
        orderCriteria.postValue(value)
    }

    fun insert(timerDef: TimerDef) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(timerDef)
    }

    fun delete(timerDef: TimerDef) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(timerDef)
    }

}
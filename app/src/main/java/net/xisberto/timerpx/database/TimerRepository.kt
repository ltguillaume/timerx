package net.xisberto.timerpx.database

import androidx.lifecycle.LiveData

class TimerRepository(private val database: Database) {

    fun getAllTimers(orderCriteria: String? = TimerDao.COLUMN_LABEL): LiveData<List<TimerDef>> {
        return when (orderCriteria) {
            TimerDao.COLUMN_DURATION -> database.timerDao().getTimersOrderByDuration()
            else -> database.timerDao().getTimersOrderByLabel()
        }
    }

    fun insert(timerDef: TimerDef) = database.timerDao().insert(timerDef)

    fun delete(timerDef: TimerDef) = database.timerDao().delete(timerDef)
}
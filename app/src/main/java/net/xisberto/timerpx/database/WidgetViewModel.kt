package net.xisberto.timerpx.database

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private const val TAG = "WidgetViewModel"

class WidgetViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: WidgetRepository
    val newId = MutableLiveData<Long>()

    init {
        val database = Database.getDatabase(application.applicationContext)
        repository = WidgetRepository(database)
    }

    fun get(timerWidgetId: Int): LiveData<WidgetAndDef> {
        val result = MutableLiveData<WidgetAndDef>()
        viewModelScope.launch(Dispatchers.IO) {
            repository.get(timerWidgetId)?.let {
                Log.d(TAG, "Got WAD with widget ${it.widget.id} - ${it.widget.appWidgetId}")
                result.postValue(it)
            } ?: run {
                Log.d(TAG, "WAD not found")
            }
        }
        return result
    }

    fun insert(timerWidget: TimerWidget) {
        viewModelScope.launch(Dispatchers.IO) {
            newId.postValue(repository.insert(timerWidget))
        }
    }

    fun delete(timerWidget: TimerWidget) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(timerWidget)
    }

}
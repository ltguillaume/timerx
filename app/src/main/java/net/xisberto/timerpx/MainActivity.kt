package net.xisberto.timerpx

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.preference.PreferenceManager
import com.google.android.material.shape.MaterialShapeDrawable
import net.xisberto.timerpx.databinding.ActivityMainBinding
import net.xisberto.timerpx.settings.SettingsActivity
import net.xisberto.timerpx.timer_list.TimerDefFragment
import net.xisberto.timerpx.util.hasAlarmApps

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        WindowCompat.setDecorFitsSystemWindows(window, true)
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.appBarLayout.statusBarForeground =
            MaterialShapeDrawable.createWithElevationOverlay(this)

        setSupportActionBar(binding.toolbar)

        if (savedInstanceState == null) {
            if (hasAlarmApps(this)) {
                supportFragmentManager.commit {
                    setReorderingAllowed(true)
                    replace<TimerDefFragment>(R.id.main_content)
                }
            } else {
                supportFragmentManager.commit {
                    setReorderingAllowed(true)
                    replace<NoAlarmAppFragment>(R.id.main_content)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        invalidateOptionsMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        val settings = PreferenceManager.getDefaultSharedPreferences(this)
        val keyOrderBy = getString(R.string.key_order_by)
        val valuesOrderBy = resources.getStringArray(R.array.values_order_by)
        if (settings.contains(keyOrderBy)) {
            when (settings.getString(keyOrderBy, valuesOrderBy[0])) {
                valuesOrderBy[0] -> menu?.findItem(R.id.item_order_by_name)?.isChecked = true
                else -> menu?.findItem(R.id.item_order_by_duration)?.isChecked = true
            }
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.menu_settings) {
            startActivity(Intent(this, SettingsActivity::class.java))
            return true
        }

        val settings = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val keyOrderBy = getString(R.string.key_order_by)
        val valuesOrderBy = resources.getStringArray(R.array.values_order_by)

        return when (item.itemId) {
            R.id.item_order_by_name -> {
                item.isChecked = true
                binding.toolbar.menu.findItem(R.id.item_order_by_duration).isChecked = false
                settings.edit()
                    .putString(keyOrderBy, valuesOrderBy[0])
                    .apply()
                true
            }

            R.id.item_order_by_duration -> {
                item.isChecked = true
                binding.toolbar.menu.findItem(R.id.item_order_by_name).isChecked = false
                settings.edit()
                    .putString(keyOrderBy, valuesOrderBy[1])
                    .apply()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}
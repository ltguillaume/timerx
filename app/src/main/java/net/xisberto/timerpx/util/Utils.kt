package net.xisberto.timerpx.util

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.os.Build
import android.provider.AlarmClock
import net.xisberto.timerpx.database.TimerDef
import java.util.*

@Suppress("DEPRECATION")
fun getAlarmApps(context: Context): List<ResolveInfo> {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        context.packageManager.queryIntentActivities(
            Intent(AlarmClock.ACTION_SET_ALARM),
            PackageManager.ResolveInfoFlags.of(PackageManager.MATCH_DEFAULT_ONLY.toLong())
        )
    } else {
        context.packageManager.queryIntentActivities(
            Intent(AlarmClock.ACTION_SET_ALARM),
            PackageManager.MATCH_DEFAULT_ONLY
        )
    }
}

fun hasAlarmApps(context: Context): Boolean {
    return true;    // Always return true, because getAlarmApps() is faulty, resulting in not being able to use alternative alarm apps when the default clock app isn't installed/enabled.
    //return getAlarmApps(context).isNotEmpty()
}

fun formatTime(timerDef: TimerDef): String {
    return formatTime(timerDef.duration)
}

fun formatTime(duration: Long): String {
    return String.format(
        "%02d:%02d",
        duration / 3600,
        (duration % 3600) / 60
    )
}

fun formatTime(calendar: Calendar): String {
    return String.format(
        "%02d:%02d",
        calendar.get(Calendar.HOUR_OF_DAY),
        calendar.get(Calendar.MINUTE)
    )
}

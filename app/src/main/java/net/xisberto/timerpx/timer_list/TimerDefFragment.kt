package net.xisberto.timerpx.timer_list

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.Icon
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.preference.PreferenceManager
import net.xisberto.timerpx.R
import net.xisberto.timerpx.alarm.AddAlarmActivity
import net.xisberto.timerpx.database.TimerDef
import net.xisberto.timerpx.database.TimerViewModel
import net.xisberto.timerpx.databinding.FragmentTimerListBinding
import net.xisberto.timerpx.util.formatTime
import java.util.concurrent.TimeUnit

/**
 * A fragment representing a list of Items.
 */
class TimerDefFragment : Fragment(), TimerDefRecyclerViewAdapter.OnTimerInteractionListener {

    private val items = ArrayList<TimerDef>()
    private val viewModel: TimerViewModel by activityViewModels()
    private val newTimerViewModel: NewTimerViewModel by activityViewModels()
    private var _binding: FragmentTimerListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTimerListBinding.inflate(inflater, container, false)
        val view = binding.root

        val action = arguments?.getInt(ARG_ACTION) ?: ACTION_MANAGE

        binding.list.adapter = TimerDefRecyclerViewAdapter(items, action, this)

        when (action) {
            ACTION_MANAGE -> binding.btnAddTimer.setOnClickListener {
                activity?.let {
                    val dialog = DurationPickerDialog()
                    dialog.show(it.supportFragmentManager, "dialog")
                }
            }

            ACTION_SELECT -> binding.btnAddTimer.visibility = View.GONE
        }

        return view
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onAttach(context: Context) {
        super.onAttach(context)

        // viewModel operations for when running under appwidget.TimerAppWidgetConfigureActivity
        viewModel.action.postValue(arguments?.getInt(ARG_ACTION) ?: ACTION_MANAGE)
        newTimerViewModel.label.observe(this) { label ->
            val duration = TimeUnit.HOURS.toSeconds(newTimerViewModel.hour.value!!.toLong()) +
                    TimeUnit.MINUTES.toSeconds(newTimerViewModel.minute.value!!.toLong())

            viewModel.insert(TimerDef(duration = duration, label = label))
        }
        viewModel.allTimers.observe(this) { timers ->
            if (timers == null) {
                return@observe
            }
            items.clear()
            if (timers.isEmpty()) {
                binding.textEmpty.visibility = View.VISIBLE
                binding.list.visibility = View.GONE
            } else {
                binding.textEmpty.visibility = View.GONE
                binding.list.visibility = View.VISIBLE
                updateShortcuts(context.applicationContext, timers)
            }
            items.addAll(timers)
            binding.list.adapter?.notifyDataSetChanged()
        }
    }

    private val prefsListener =
        SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences: SharedPreferences, key: String? ->
            if (getString(R.string.key_order_by) == key) {
                updateOrderCriteriaFromPreferences(sharedPreferences)
            }
        }

    override fun onStart() {
        super.onStart()
        val sharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(requireContext().applicationContext)
        updateOrderCriteriaFromPreferences(sharedPreferences)
        sharedPreferences.registerOnSharedPreferenceChangeListener(prefsListener)
    }

    override fun onStop() {
        super.onStop()
        PreferenceManager.getDefaultSharedPreferences(requireContext().applicationContext)
            .unregisterOnSharedPreferenceChangeListener(prefsListener)
    }

    private fun updateOrderCriteriaFromPreferences(sharedPreferences: SharedPreferences) {
        val keyOrderBy = getString(R.string.key_order_by)
        val valuesOrderBy = resources.getStringArray(R.array.values_order_by)
        val valueOrderBy = sharedPreferences.getString(keyOrderBy, valuesOrderBy[0])
        viewModel.orderTimers(valueOrderBy)
    }

    override fun onTimerClicked(timerDef: TimerDef) {
        when (viewModel.action.value) {
            ACTION_MANAGE -> {
                val intent = Intent(activity, AddAlarmActivity::class.java).apply {
                    putExtra(AddAlarmActivity.EXTRA_DELAY, timerDef.duration)
                    putExtra(AddAlarmActivity.EXTRA_LABEL, timerDef.label)
                }
                startActivity(intent)
            }

            ACTION_SELECT -> {
                viewModel.selection.postValue(timerDef)
            }
        }
    }

    override fun onTimerRemovedClicked(timerDef: TimerDef) {
        if (viewModel.action.value == ACTION_MANAGE) {
            viewModel.delete(timerDef)
        }
    }

    private fun updateShortcuts(context: Context, list: List<TimerDef>) {
        val shortcutManager = context.getSystemService(ShortcutManager::class.java)
        val shortcuts = ArrayList<ShortcutInfo>()
        for (item in list) {
            shortcuts.add(
                ShortcutInfo.Builder(context, item.duration.toString())
                    .setShortLabel("%s %s".format(formatTime(item), item.label))
                    .setIcon(Icon.createWithResource(context, R.drawable.ic_shortcut_access_time))
                    .setIntent(getIntent(context, item))
                    .build()
            )
            if (shortcuts.size > 4) {
                break
            }
        }
        shortcutManager!!.dynamicShortcuts = shortcuts
    }

    companion object {

        const val ACTION_MANAGE = 0
        const val ACTION_SELECT = 1
        const val ARG_ACTION = "action"

        @JvmStatic
        fun newInstance(action: Int = ACTION_MANAGE): TimerDefFragment {
            val fragment = TimerDefFragment()
            val args = Bundle()
            args.putInt(ARG_ACTION, action)
            fragment.arguments = args
            return fragment
        }
    }
}

internal fun getIntent(context: Context, timerDef: TimerDef): Intent {
    return Intent(context, AddAlarmActivity::class.java)
        .setAction("${context.getString(R.string.app_name)}${timerDef.duration}")
        .putExtra(AddAlarmActivity.EXTRA_DELAY, timerDef.duration)
        .putExtra(AddAlarmActivity.EXTRA_LABEL, timerDef.label)
        .setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS or Intent.FLAG_ACTIVITY_CLEAR_TASK)
}